#!/bin/bash
# Script used to update running container with latest image from docker hub

docker stop $(docker ps -aq)
docker rm $(docker ps -aq)
docker rmi $(docker images -aq)
docker run --name my-cv -dit -p 80:80 --restart unless-stopped fsabol/mycv

